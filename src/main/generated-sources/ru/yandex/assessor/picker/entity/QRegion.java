package ru.yandex.assessor.picker.entity;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QRegion is a Querydsl query type for Region
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRegion extends EntityPathBase<Region> {

    private static final long serialVersionUID = 825341172L;

    public static final QRegion region = new QRegion("region");

    public final NumberPath<Double> latitude = createNumber("latitude", Double.class);

    public final NumberPath<Double> longitude = createNumber("longitude", Double.class);

    public final NumberPath<Long> regionId = createNumber("regionId", Long.class);

    public final StringPath regionNameEng = createString("regionNameEng");

    public final StringPath regionNameRu = createString("regionNameRu");

    public QRegion(String variable) {
        super(Region.class, forVariable(variable));
    }

    public QRegion(Path<? extends Region> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRegion(PathMetadata<?> metadata) {
        super(Region.class, metadata);
    }

}

