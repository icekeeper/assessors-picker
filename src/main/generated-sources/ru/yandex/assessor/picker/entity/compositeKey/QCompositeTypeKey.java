package ru.yandex.assessor.picker.entity.compositeKey;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCompositeTypeKey is a Querydsl query type for CompositeTypeKey
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QCompositeTypeKey extends BeanPath<CompositeTypeKey> {

    private static final long serialVersionUID = -695617708L;

    public static final QCompositeTypeKey compositeTypeKey = new QCompositeTypeKey("compositeTypeKey");

    public final NumberPath<Integer> subtypeId = createNumber("subtypeId", Integer.class);

    public final NumberPath<Integer> typeId = createNumber("typeId", Integer.class);

    public QCompositeTypeKey(String variable) {
        super(CompositeTypeKey.class, forVariable(variable));
    }

    public QCompositeTypeKey(Path<? extends CompositeTypeKey> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCompositeTypeKey(PathMetadata<?> metadata) {
        super(CompositeTypeKey.class, metadata);
    }

}

