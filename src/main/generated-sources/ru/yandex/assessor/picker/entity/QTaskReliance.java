package ru.yandex.assessor.picker.entity;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QTaskReliance is a Querydsl query type for TaskReliance
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTaskReliance extends EntityPathBase<TaskReliance> {

    private static final long serialVersionUID = -1963165980L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTaskReliance taskReliance = new QTaskReliance("taskReliance");

    public final NumberPath<Double> accuracy = createNumber("accuracy", Double.class);

    public final QAssessor assessor;

    public final ru.yandex.assessor.picker.entity.compositeKey.QCompositeRelianceKey key;

    public final NumberPath<Integer> subtypeId = createNumber("subtypeId", Integer.class);

    public final NumberPath<Integer> typeId = createNumber("typeId", Integer.class);

    public QTaskReliance(String variable) {
        this(TaskReliance.class, forVariable(variable), INITS);
    }

    public QTaskReliance(Path<? extends TaskReliance> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTaskReliance(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QTaskReliance(PathMetadata<?> metadata, PathInits inits) {
        this(TaskReliance.class, metadata, inits);
    }

    public QTaskReliance(Class<? extends TaskReliance> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.assessor = inits.isInitialized("assessor") ? new QAssessor(forProperty("assessor")) : null;
        this.key = inits.isInitialized("key") ? new ru.yandex.assessor.picker.entity.compositeKey.QCompositeRelianceKey(forProperty("key")) : null;
    }

}

