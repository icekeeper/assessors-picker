package ru.yandex.assessor.picker.entity.compositeKey;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QCompositeRelianceKey is a Querydsl query type for CompositeRelianceKey
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QCompositeRelianceKey extends BeanPath<CompositeRelianceKey> {

    private static final long serialVersionUID = 713058479L;

    public static final QCompositeRelianceKey compositeRelianceKey = new QCompositeRelianceKey("compositeRelianceKey");

    public final NumberPath<Long> assessor = createNumber("assessor", Long.class);

    public final NumberPath<Integer> subtypeId = createNumber("subtypeId", Integer.class);

    public final NumberPath<Integer> typeId = createNumber("typeId", Integer.class);

    public QCompositeRelianceKey(String variable) {
        super(CompositeRelianceKey.class, forVariable(variable));
    }

    public QCompositeRelianceKey(Path<? extends CompositeRelianceKey> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCompositeRelianceKey(PathMetadata<?> metadata) {
        super(CompositeRelianceKey.class, metadata);
    }

}

