package ru.yandex.assessor.picker.entity;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAssessor is a Querydsl query type for Assessor
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAssessor extends EntityPathBase<Assessor> {

    private static final long serialVersionUID = -1098955193L;

    public static final QAssessor assessor = new QAssessor("assessor");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> karmaNegativeScore = createNumber("karmaNegativeScore", Integer.class);

    public final NumberPath<Integer> karmaPositiveScore = createNumber("karmaPositiveScore", Integer.class);

    public final NumberPath<Integer> numberOfSkulls = createNumber("numberOfSkulls", Integer.class);

    public final NumberPath<Integer> numberOfStars = createNumber("numberOfStars", Integer.class);

    public final BooleanPath online = createBoolean("online");

    public final NumberPath<Long> regionId = createNumber("regionId", Long.class);

    public final ListPath<TaskReliance, QTaskReliance> taskReliances = this.<TaskReliance, QTaskReliance>createList("taskReliances", TaskReliance.class, QTaskReliance.class, PathInits.DIRECT2);

    public QAssessor(String variable) {
        super(Assessor.class, forVariable(variable));
    }

    public QAssessor(Path<? extends Assessor> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAssessor(PathMetadata<?> metadata) {
        super(Assessor.class, metadata);
    }

}

