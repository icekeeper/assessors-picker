package ru.yandex.assessor.picker.entity;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QTaskType is a Querydsl query type for TaskType
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTaskType extends EntityPathBase<TaskType> {

    private static final long serialVersionUID = -1135870881L;

    public static final QTaskType taskType = new QTaskType("taskType");

    public final NumberPath<Integer> subtypeId = createNumber("subtypeId", Integer.class);

    public final NumberPath<Integer> typeId = createNumber("typeId", Integer.class);

    public final StringPath typeNameEng = createString("typeNameEng");

    public final StringPath typeNameRu = createString("typeNameRu");

    public QTaskType(String variable) {
        super(TaskType.class, forVariable(variable));
    }

    public QTaskType(Path<? extends TaskType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTaskType(PathMetadata<?> metadata) {
        super(TaskType.class, metadata);
    }

}

