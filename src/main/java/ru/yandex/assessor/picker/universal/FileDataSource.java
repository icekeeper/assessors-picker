package ru.yandex.assessor.picker.universal;

import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.yandex.assessor.picker.entity.*;
import ru.yandex.assessor.picker.entity.compositeKey.CompositeRelianceKey;
import ru.yandex.assessor.picker.repository.AssessorRepository;
import ru.yandex.assessor.picker.controller.AssessorController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Степан on 13.11.2014.
 */

@Service
@Configurable
public class FileDataSource implements UpdateDataSource {

    @Autowired
    AssessorRepository assessorRepository;
    @PersistenceContext
    EntityManager entityManager;

    public FileDataSource(AssessorRepository assessorRepository) {

        this.assessorRepository = assessorRepository;

    }

    public FileDataSource() {
    }

    @Override
    public void getAssessors(DataCallback sender) {
        Random rand = new Random();
        int part = 0;
        List<String> arrayList = new ArrayList<>();
        List<Assessor> assessorsList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/assessors.tsv"), "utf-8"))) {
            String line = reader.readLine();
            while (line != null) {
                part = part + 1;
                arrayList.add(line);
                if (part % 1000 == 0) {
                    //начинаем парсить строки
                    for (String tmp : arrayList) {
                        String[] p;
                        p = tmp.split("\t");
                        assessorsList.add(
                                new Assessor(
                                        Long.valueOf(p[0]),
                                        Long.valueOf(p[1]),
                                        Integer.valueOf(p[5]),
                                        Integer.valueOf(p[4]),
                                        Integer.valueOf(p[2]),
                                        Integer.valueOf(p[3]),
                                        rand.nextBoolean()
                                )
                        );

                    }

                    sender.process(assessorsList);
                    arrayList = new ArrayList<>();
                    assessorsList = new ArrayList<>();
                    //добавление элементов в список assessorsList
                    //чистка двух списков
                }
                //если прочитано 1000 строк,то вызываем метод getData(arrayList),который закинет эти 1000 строк в БД
                //потом удаляем все элементы из списка
                line = reader.readLine();
            }
            for (String tmp : arrayList) {
                String[] p;
                p = tmp.split("\t");
                assessorsList.add(
                        new Assessor(
                                Long.valueOf(p[0]),
                                Long.valueOf(p[1]),
                                Integer.valueOf(p[5]),
                                Integer.valueOf(p[4]),
                                Integer.valueOf(p[2]),
                                Integer.valueOf(p[3]),
                                rand.nextBoolean()
                        )
                );

            }
            sender.process(assessorsList);
            //закидываем в БД оставшиеся строки,которых меньше 1000
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void getRegion(DataCallback sender) {

        List<String> arrayList = new ArrayList<>();
        List<Region> regionList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/regions.tsv"), "utf-8"))) {
            String line = reader.readLine();
            while (line != null) {
                arrayList.add(line);
                line = reader.readLine();
            }

            for (String tmp : arrayList) {
                String[] p;
                p = tmp.split("\t");
                regionList.add(
                        new Region(
                                Long.valueOf(p[0]),
                                p[1],
                                p[2],
                                Double.valueOf(p[3]),
                                Double.valueOf(p[4])));
            }

            sender.process(regionList);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void getTaskType(DataCallback sender) {
        List<String> arrayList = new ArrayList<>();
        List<TaskType> taskTypeList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/task_types.tsv"), "utf-8"))) {
            String line = reader.readLine();
            while (line != null) {
                arrayList.add(line);
                line = reader.readLine();
            }

            for (String tmp : arrayList) {
                String[] p;
                p = tmp.split("\t");
                taskTypeList.add(
                        new TaskType(
                                Integer.valueOf(p[0]),
                                Integer.valueOf(p[1]),
                                p[2],
                                p[3]));
            }

            sender.process(taskTypeList);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void getTaskReliance(DataCallback sender) {


        try { //считаем с базы данных сущность асессор для последующей инициализации


            List<Assessor> assessorList = new ArrayList<>();

            assessorList = Lists.newArrayList(assessorRepository.findAll());

            Map<Long, Assessor> assessorLong = new HashMap<Long, Assessor>();


            for (Assessor tmp : assessorList) {

                assessorLong.put(tmp.getId(), tmp);

            }

            int part = 0;
            List<String> arrayList = new ArrayList<>();
            List<TaskReliance> taskRelianceList = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/user_reliance.tsv"), "utf-8"))) {
                String line = reader.readLine();
                while (line != null) {
                    part = part + 1;
                    arrayList.add(line);
                    if (part % 1000 == 0) {
                        //начинаем парсить строки
                        try {
                            for (String tmp : arrayList) {
                                String[] p;
                                p = tmp.split("\t");
                                taskRelianceList.add(
                                        new TaskReliance(
                                                assessorLong.get(Long.valueOf(p[0])),
                                                new CompositeRelianceKey(
                                                        Long.valueOf(p[0]),
                                                        Integer.valueOf(p[1]),
                                                        Integer.valueOf(p[2])),

                                                Double.valueOf(p[3])

                                        )
                                );

                            }
                        } catch (NullPointerException e) {
                            System.out.println("error in getTaskReliance().1");
                            e.printStackTrace();
                        }

                        sender.process(taskRelianceList);
                        arrayList = new ArrayList<>();
                        taskRelianceList = new ArrayList<>();
                        //добавление элементов в список assessorsList
                        //чистка двух списков
                    }
                    //если прочитано 1000 строк,то вызываем метод getData(arrayList),который закинет эти 1000 строк в БД
                    //потом удаляем все элементы из списка
                    line = reader.readLine();
                }
                try {
                    for (String tmp : arrayList) {
                        String[] p;
                        p = tmp.split("\t");
                        taskRelianceList.add(
                                new TaskReliance(
                                        assessorLong.get(p[0]),
                                        new CompositeRelianceKey(
                                                Long.valueOf(p[0]),
                                                Integer.valueOf(p[1]),
                                                Integer.valueOf(p[2])),
                                        Double.valueOf(p[3])

                                )
                        );

                    }
                } catch (NullPointerException e) {
                    System.out.println("error in getTaskReliance().2");
                    e.printStackTrace();
                }
                sender.process(taskRelianceList);
                //закидываем в БД оставшиеся строки,которых меньше 1000
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            System.out.println("error in getTaskReliance().3");
            e.printStackTrace();
        }

    }
}
