package ru.yandex.assessor.picker.universal;

import ru.yandex.assessor.picker.entity.TaskReliance;

/**
 * Created by Степан on 17.11.2014.
 */
public interface UpdateDataSource {
    void getAssessors(DataCallback sender);
    void getRegion(DataCallback sender);
    void getTaskType(DataCallback sender);
    void getTaskReliance(DataCallback<TaskReliance> sender);
}
