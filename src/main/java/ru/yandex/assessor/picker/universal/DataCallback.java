package ru.yandex.assessor.picker.universal;

import java.util.List;

/**
 * Created by Степан on 17.11.2014.
 */
public interface DataCallback<T> {
    void process(List<T> list);
}
