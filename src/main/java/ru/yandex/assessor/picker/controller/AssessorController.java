package ru.yandex.assessor.picker.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.yandex.assessor.picker.dto.MapAssessorsDTO;
import ru.yandex.assessor.picker.dto.SubtypeDTO;
import ru.yandex.assessor.picker.dto.TaskCategoryFilter;
import ru.yandex.assessor.picker.dto.TaskTypeDTO;
import ru.yandex.assessor.picker.entity.*;
import ru.yandex.assessor.picker.repository.AssessorRepository;
import ru.yandex.assessor.picker.repository.RegionRepository;
import ru.yandex.assessor.picker.repository.TaskRelianceRepository;
import ru.yandex.assessor.picker.repository.TaskTypeRepository;
import ru.yandex.assessor.picker.universal.DataCallback;
import ru.yandex.assessor.picker.universal.FileDataSource;
import ru.yandex.assessor.picker.universal.UpdateDataSource;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.*;

@RestController
public class AssessorController {


    @Autowired
    RegionRepository regionRepository;
    @Autowired
    AssessorRepository assessorRepository;
    @Autowired
    TaskTypeRepository taskTypeRepository;
    @Autowired
    TaskRelianceRepository taskRelianceRepository;
    @PersistenceContext
    EntityManager entityManager;
    ObjectMapper mapper = new ObjectMapper();


    @RequestMapping("/addAssessorInDB")
    public String addAssessorInDB() {
        UpdateDataSource assessor = new FileDataSource();
        assessor.getAssessors(assessorRepository::save);
//        assessor.getAssessors(new DataCallback() {
//            @Override
//            public void process(List list) {
//                assessorRepository.save(list);
//            }
//        });

        return "Assessors successfully added";
    }

    @RequestMapping("/addRegionInDB")
    public String addRegionInDB() {
        UpdateDataSource regions = new FileDataSource();
        regions.getRegion(regionRepository::save);
        return "Region successfully added";
    }

    @RequestMapping("/addTaskTypeInDB")
    public String addTaskTypeInDB() {
        UpdateDataSource tasks = new FileDataSource();
        tasks.getTaskType(taskTypeRepository::save);

        return "Task successfully added ";
    }

    @RequestMapping("/addTaskRelianceInDB")
    public String addTaskRelianceInDB() {

        UpdateDataSource reliance = new FileDataSource(assessorRepository);
        reliance.getTaskReliance(taskRelianceRepository::save);
//        reliance.getTaskReliance(new DataCallback<TaskReliance>() {
//            @Override
//            public void process(List<TaskReliance> list) {
//                for (TaskReliance taskReliance : list) {
//                    taskReliance.getAssessor();
//                }
//                taskRelianceRepository.save(list);
//            }
//        });

        return "Task Reliance successfully added ";
    }


//assessors?karma_positive_number=2&karma_negative_number=9999&skull_from_number=0&skull_to_number=9999
// &star_from_number=0&star_to_number=9999&online=true&region_id=2&page=0&size=15

    //http://localhost:3000/assessors?score_positive_number=0&score_negative_number=9999&skull_from_number=0&skull_to_number=9999&star_from_number=0&star_to_number=9999&online=nothing
    // &task_categories=[{"typeId":"44","subtypeId":"-1","accuracy":"1"},{"typeId":"50","subtypeId":"168","accuracy":"31.99"},{"typeId":"66","subtypeId":"-1","accuracy":"0.01"}]&region_id=2&page=1&size=15&total_pages=1


    @RequestMapping("/assessors")

    public PageImpl<Assessor> getAssessorData(@RequestParam(value = "karma_positive_number", required = false, defaultValue = "-1") int karmaPositiveNumber, @RequestParam(value = "karma_negative_number", required = false, defaultValue = "-1") int karmaNegativeNumber,
                                              @RequestParam(value = "skull_from_number", required = false, defaultValue = "-1") int skullFromNumber, @RequestParam(value = "skull_to_number", required = false, defaultValue = "1000") int skullToNumber,
                                              @RequestParam(value = "star_from_number", required = false, defaultValue = "-1") int starFromNumber, @RequestParam(value = "star_to_number", required = false, defaultValue = "10000") int starToNumber,
                                              @RequestParam(value = "online", required = false, defaultValue = "nothing") String online, @RequestParam(value = "region_id", required = false, defaultValue = "-1") long regionId,
                                              @RequestParam(value = "task_categories", required = false, defaultValue = "[]") String json,
                                              @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "size", required = false, defaultValue = "-1") int size) {

        QAssessor assessor = QAssessor.assessor;
        QTaskReliance taskReliance = QTaskReliance.taskReliance;

        Random rand = new Random();

//прочитать про http, get post запросы ,mapping параметров
        BooleanExpression condition = assessor.numberOfSkulls.between(skullFromNumber, skullToNumber)
                .and(assessor.numberOfStars.between(starFromNumber, starToNumber));

        if (karmaPositiveNumber != -1) {
            condition = condition.and(assessor.karmaPositiveScore.goe(karmaPositiveNumber));
        }

        if (karmaNegativeNumber != -1) {
            condition = condition.and(assessor.karmaNegativeScore.loe(karmaNegativeNumber));
        }
        //надо ли обрабатывать случай,когда передается online!=nothing && online!=true && online!=false
        //если online=чему угодно,то Boolean.valueOf(online)=false
        switch (online) {
            case "true":
                condition = condition.and(assessor.online.eq(true));
                break;
            case "false":
                condition = condition.and(assessor.online.eq(false));
                break;
        }
        //теперь,если online=чему угодно,то выберутся и true и false

        if (regionId != -1) {
            condition = condition.and(assessor.regionId.eq(regionId));
        }


        BooleanExpression condition3 = taskReliance.key.typeId.eq(0);//для невозможного события
        BooleanExpression condition4 = taskReliance.key.typeId.gt(0);//инициализация condition4 для того,чтобы выбрать всех асессоров

        List<TaskCategoryFilter> taskCategory = new ArrayList<>();

        //десериализуем строку формата json в класс
        if (json.compareTo("[]") != 0) {

            try {

                //ArrayList<TaskCategoryFilter> taskCategory = mapper.readValue(task_categories, TypeFactory.defaultInstance().constructArrayType(TaskCategoryFilter.class));
                taskCategory = mapper.readValue(json, new TypeReference<List<TaskCategoryFilter>>() {
                });
                for (TaskCategoryFilter filter : taskCategory) {
                    BooleanExpression condition2 = taskReliance.key.typeId.eq(filter.getTypeId());
                    condition2 = condition2.and(taskReliance.key.subtypeId.eq(filter.getSubtypeId()));
                    condition2 = condition2.and(taskReliance.accuracy.goe(filter.getAccuracy()));
                    condition3 = condition3.or(condition2);

                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                System.out.println("!!!!!");
            }


        } else {

            condition3 = condition4;//если task_categories не заданы,то результат выбора асессора не зависит от типа задания
        }


        long sizeOfList = new JPAQuery(entityManager)
                .from(assessor)
                .innerJoin(assessor.taskReliances, taskReliance)
                .on(condition3)
                .where(condition)
                .count();

        if (size == -1) {
            size = Math.max(1, (int) sizeOfList);
        }


        List<Assessor> assessorList = new JPAQuery(entityManager)
                .from(assessor)
                .innerJoin(assessor.taskReliances, taskReliance)
                .on(condition3)
                .where(condition)
                .orderBy(assessor.id.asc())
                .distinct()
                .limit(size)
                .offset((page - 1) * size)
                .list(assessor);


        /*for (Assessor entityForOnline : assessorList) {

            entityForOnline.setOnline(rand.nextBoolean());
        }*/


        if (json.compareTo("[]") != 0) {
            for (Assessor entity : assessorList) {

                List<TaskReliance> requiredTasks = new ArrayList<>();

                for (TaskReliance allTask : entity.getTaskReliances()) {


                    for (TaskCategoryFilter filter : taskCategory) {

                        if ((allTask.getKey().getTypeId() == filter.getTypeId()) && (allTask.getKey().getSubtypeId() == filter.getSubtypeId()) && (allTask.getAccuracy() >= filter.getAccuracy())) {
                            requiredTasks.add(allTask);
                        }

                    }

                }

                entity.getTaskReliances().retainAll(requiredTasks);

            }
        }


        PageRequest pageable = new PageRequest(page, size);
        return (new PageImpl<>(assessorList, pageable, sizeOfList));
    }

    @RequestMapping("/regions")
    public List<Region> findAllRegion() {

        return Lists.newArrayList(regionRepository.findAll());

    }

    //assessors?karma_positive_number=2&karma_negative_number=9999&skull_from_number=0&skull_to_number=9999
    // &star_from_number=0&star_to_number=9999&online=true&region_id=2

    //&task_categories=[{"typeId":"44","subtypeId":"-1","accuracy":"1"},{"typeId":"50","subtypeId":"168","accuracy":"31.99"},{"typeId":"66","subtypeId":"-1","accuracy":"0.01"}]&region_id=2&page=1&size=15&total_pages=1

    @RequestMapping("/map")

    public List<MapAssessorsDTO> getRegionOnMap(@RequestParam(value = "karma_positive_number", required = false, defaultValue = "-1") int karmaPositiveNumber, @RequestParam(value = "karma_negative_number", required = false, defaultValue = "-1") int karmaNegativeNumber,
                                                @RequestParam(value = "skull_from_number", required = false, defaultValue = "-1") int skullFromNumber, @RequestParam(value = "skull_to_number", required = false, defaultValue = "1000") int skullToNumber,
                                                @RequestParam(value = "star_from_number", required = false, defaultValue = "-1") int starFromNumber, @RequestParam(value = "star_to_number", required = false, defaultValue = "10000") int starToNumber,
                                                @RequestParam(value = "online", required = false, defaultValue = "nothing") String online, @RequestParam(value = "region_id", required = false, defaultValue = "-1") long regionId,
                                                @RequestParam(value = "task_categories", required = false, defaultValue = "[]") String json) {

        QAssessor assessor = QAssessor.assessor;
        QTaskReliance taskReliance = QTaskReliance.taskReliance;
        Random rand = new Random();

        BooleanExpression condition = assessor.numberOfSkulls.between(skullFromNumber, skullToNumber)
                .and(assessor.numberOfStars.between(starFromNumber, starToNumber));

        if (karmaPositiveNumber != -1) {
            condition = condition.and(assessor.karmaPositiveScore.goe(karmaPositiveNumber));
        }

        if (karmaNegativeNumber != -1) {
            condition = condition.and(assessor.karmaNegativeScore.loe(karmaNegativeNumber));
        }

        try {
            if ((online.compareTo("nothing") != 0) && (online.compareTo("true") != 0) && (online.compareTo("false") != 0)) {
                throw new Exception("There is exception in param /map?online!");
            }

            if ("nothing".equals(online)) {
                condition = condition.and(assessor.online.between(false, true));
            } else condition = condition.and(assessor.online.eq(Boolean.valueOf(online)));

        } catch (Exception e) {
            System.err.println(e.getMessage() + " online = " + online);
        }


        if (regionId != -1) {
            condition = condition.and(assessor.regionId.eq(regionId));
        }

        /*new JPAUpdateClause(entityManager, assessor)
                .set(assessor.online, rand.nextBoolean())
                .execute();*/

        BooleanExpression condition3 = taskReliance.key.typeId.eq(0);//для невозможного события
        BooleanExpression condition4 = taskReliance.key.typeId.gt(0);//инициализация condition4 для того,чтобы выбрать всех асессоров

        List<TaskCategoryFilter> taskCategory = new ArrayList<>();

        //десериализуем строку формата json в класс
        if (json.compareTo("[]") != 0) {

            try {

                //ArrayList<TaskCategoryFilter> taskCategory = mapper.readValue(task_categories, TypeFactory.defaultInstance().constructArrayType(TaskCategoryFilter.class));
                taskCategory = mapper.readValue(json, new TypeReference<List<TaskCategoryFilter>>() {
                });
                for (TaskCategoryFilter filter : taskCategory) {
                    BooleanExpression condition2 = taskReliance.key.typeId.eq(filter.getTypeId());
                    condition2 = condition2.and(taskReliance.key.subtypeId.eq(filter.getSubtypeId()));
                    condition2 = condition2.and(taskReliance.accuracy.goe(filter.getAccuracy()));
                    condition3 = condition3.or(condition2);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            condition3 = condition4;//если task_categories не заданы,то результат выбора асессора не зависит от типа задания
        }


        long countOfAllAssessor = new JPAQuery(entityManager)
                .from(assessor)
                .innerJoin(assessor.taskReliances, taskReliance)
                .on(condition3)
                .where(condition)
                .count();//получил общее число асессоров,соответствующих фильтру

        List<Long> listOfRegionId = new JPAQuery(entityManager)
                .from(assessor)
                .innerJoin(assessor.taskReliances, taskReliance)
                .on(condition3)
                .where(condition)
                .list(assessor.regionId);//получил список всех region_id, соответствующих фильтру

        HashSet<Long> listOfUniqueRegionId = new HashSet<>();


        List<MapAssessorsDTO> regionOnMap = new ArrayList<>();

        for (Long tmp : listOfRegionId) {

            listOfUniqueRegionId.add(tmp); //получил список уникальных region_id
        }

        for (Long tmp : listOfUniqueRegionId) {

            //проценты-это (Collections.frequency(listOfRegionId, tmp) / countOfAllAssessor))-пробежался по списку всех region_id и, используя уникальные, посчитал процентное соотношение
            Region regionInfo = regionRepository.findOne(tmp);
            regionOnMap.add(new MapAssessorsDTO(
                    regionInfo.getRegionNameRu(),
                    regionInfo.getLatitude(),
                    regionInfo.getLongitude(),
                    (double) Collections.frequency(listOfRegionId, tmp) / countOfAllAssessor));

        }

        return regionOnMap;

    }

    @RequestMapping("/task-categories")
    public List<TaskTypeDTO> taskCategories() {

        List<TaskTypeDTO> taskTypeDTOList = new ArrayList<>();
        HashSet<Integer> alreadyAddedThisTypeId = new HashSet<>();

        //пробегамся по всем элементам таблицы TaskType
        for (TaskType temp : taskTypeRepository.findAll()) {

            //проверяем не пробегались ли мы по детям элемента с выбранным taskId
            if (alreadyAddedThisTypeId.contains(temp.getTypeId())) continue;

            //если не пробегались,то добавляем этот новый элемент в множество уже пройденных
            alreadyAddedThisTypeId.add(temp.getTypeId());

            List<SubtypeDTO> test = new ArrayList<>();

            //теперь нужно выдернуть из поля taskNameRu="Тип задания №50 (221)" сущности TaskType 2 строки
            //1)Тип задания №50,2) (221)

            if (temp.getTypeNameRu().contains("(")) {
                //найдем начало позиции второй подстроки
                int mainStartRU = temp.getTypeNameRu().indexOf("(");
                int mainStartEng = temp.getTypeNameEng().indexOf("(");
                //пробегаемся еще раз по таблице,чтобы найти всех детей taskId, выбранного в первом цикле for
                for (TaskType temp2 : taskTypeRepository.findAll()) {

                    //здесь происходит разбиение строки taskNameRu на две части и составление списка детей taskId,выбранного в первом цикле for
                    if ((temp.getTypeId() == temp2.getTypeId()) && (temp2.getTypeNameRu().contains("("))) {

                        String nameRu = temp2.getTypeNameRu();
                        String nameEng = temp2.getTypeNameEng();
                        int startEng = nameEng.indexOf("(");
                        int startRu = nameRu.indexOf("(");
                        String subNameRU = nameRu.substring(startRu);
                        String subNameEng = nameEng.substring(startEng);
                        test.add(new SubtypeDTO(temp2.getSubtypeId(), subNameRU, subNameEng));
                    }
                }


                //теперь у нас есть taskId, выбранный в первой цикле for, и список его детей
                taskTypeDTOList.add(
                        new TaskTypeDTO(
                                temp.getTypeId(),
                                temp.getTypeNameRu().substring(0, mainStartRU),
                                temp.getTypeNameEng().substring(0, mainStartEng),
                                test)
                );
            } else {
                taskTypeDTOList.add(
                        new TaskTypeDTO(
                                temp.getTypeId(),
                                temp.getTypeNameRu(),
                                temp.getTypeNameEng(),
                                test)
                );
            }


        }

        return taskTypeDTOList;

    }

    @RequestMapping("/taskType")
    public List<TaskType> taskType() {
        return Lists.newArrayList(taskTypeRepository.findAll());
    }

    @RequestMapping("/allAssessors")
    public List<Assessor> getAllAssessors(){return Lists.newArrayList(assessorRepository.findAll());}

    @RequestMapping("/taskReliance")
    public List<TaskReliance> taskReliance() {
        return Lists.newArrayList(taskRelianceRepository.findAll());
    }

}