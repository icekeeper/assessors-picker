package ru.yandex.assessor.picker.dto;

/**
 * Created by Степан on 24.11.2014.
 */
public class SubtypeDTO {
    private int subtypeId;
    private String subtypeNameRu;
    private String subtypeNameEng;

    public SubtypeDTO() {
    }

    public SubtypeDTO(int subtypeId, String subtypeNameRu, String subtypeNameEng) {
        this.subtypeId = subtypeId;
        this.subtypeNameRu = subtypeNameRu;
        this.subtypeNameEng = subtypeNameEng;
    }

    public int getSubtypeId() {
        return subtypeId;
    }

    public void setSubtypeId(int subtypeId) {
        this.subtypeId = subtypeId;
    }

    public String getSubtypeNameRu() {
        return subtypeNameRu;
    }

    public void setSubtypeNameRu(String subtypeNameRu) {
        this.subtypeNameRu = subtypeNameRu;
    }

    public String getSubtypeNameEng() {
        return subtypeNameEng;
    }

    public void setSubtypeNameEng(String subtypeNameEng) {
        this.subtypeNameEng = subtypeNameEng;
    }
}
