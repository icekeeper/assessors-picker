package ru.yandex.assessor.picker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Степан on 23.11.2014.
 */
public class TaskCategoryFilter {
    @JsonProperty("typeId")
    private int typeId;
    @JsonProperty("subtypeId")
    private int subtypeId;
    @JsonProperty("accuracy")
    private double accuracy;

    public TaskCategoryFilter() {
    }

    public TaskCategoryFilter(int typeId, int subtypeId, double accuracy) {
        this.typeId = typeId;
        this.subtypeId = subtypeId;
        this.accuracy = accuracy;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getSubtypeId() {
        return subtypeId;
    }

    public void setSubtypeId(int subtypeId) {
        this.subtypeId = subtypeId;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }
}
