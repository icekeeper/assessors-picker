package ru.yandex.assessor.picker.dto;

/**
 * Created by Степан on 06.11.2014.
 */
public class MapAssessorsDTO {
    private String region;
    private double latitude;
    private double longitude;
    private double percentOfAssessors;

    public MapAssessorsDTO() {
    }

    public MapAssessorsDTO(String region, double latitude, double longitude, double percentOfAssessors) {
        this.region = region;
        this.latitude = latitude;
        this.longitude = longitude;
        this.percentOfAssessors = percentOfAssessors;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getPercentOfAssessors() {
        return percentOfAssessors;
    }

    public void setPercentOfAssessors(double percentOfAssessors) {
        this.percentOfAssessors = percentOfAssessors;
    }
}
