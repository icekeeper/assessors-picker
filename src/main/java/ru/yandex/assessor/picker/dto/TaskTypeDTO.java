package ru.yandex.assessor.picker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Степан on 24.11.2014.
 */
public class TaskTypeDTO {
    @JsonProperty("typeId")
    private int typeId;
    @JsonProperty("typeNameRu")
    private String typeNameRu;
    @JsonProperty("typeNameEng")
    private String typeNameEng;
    @JsonProperty("child_task_categories")
    private List<SubtypeDTO> childTaskCategories;

    public TaskTypeDTO() {
    }

    public TaskTypeDTO(int typeId, String typeNameRu, String typeNameEng, List<SubtypeDTO> childTaskCategories) {
        this.typeId = typeId;
        this.typeNameRu = typeNameRu;
        this.typeNameEng = typeNameEng;
        this.childTaskCategories = childTaskCategories;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeNameRu() {
        return typeNameRu;
    }

    public void setTypeNameRu(String typeNameRu) {
        this.typeNameRu = typeNameRu;
    }

    public String getTypeNameEng() {
        return typeNameEng;
    }

    public void setTypeNameEng(String typeNameEng) {
        this.typeNameEng = typeNameEng;
    }

    public List<SubtypeDTO> getChildTaskCategories() {
        return childTaskCategories;
    }

    public void setChildTaskCategories(List<SubtypeDTO> childTaskCategories) {
        this.childTaskCategories = childTaskCategories;
    }
}
