package ru.yandex.assessor.picker.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Степан on 30.11.2014.
 */
public class AssessorsTaskDTO {
    @JsonProperty("id")
    private long id;
    @JsonProperty("region_id")
    private long regionId;
    @JsonProperty("number_of_stars")
    private int numberOfStars;
    @JsonProperty("number_of_skulls")
    private int numberOfSkulls;
    @JsonProperty("karma_negative_score")
    private int karmaNegativeScore;
    @JsonProperty("karma_positive_score")
    private int karmaPositiveScore;
    @JsonProperty("online")
    private boolean online;
    @JsonProperty("categories-task")
    private List<TaskCategoryFilter> taskCategories;

    public AssessorsTaskDTO() {
    }

    public AssessorsTaskDTO(long id, long regionId, int numberOfStars, int numberOfSkulls, int karmaNegativeScore, int karmaPositiveScore,
                            boolean online, List<TaskCategoryFilter> taskCategories) {
        this.id = id;
        this.regionId = regionId;
        this.numberOfStars = numberOfStars;
        this.numberOfSkulls = numberOfSkulls;
        this.karmaNegativeScore = karmaNegativeScore;
        this.karmaPositiveScore = karmaPositiveScore;
        this.online = online;
        this.taskCategories = taskCategories;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public int getNumberOfStars() {
        return numberOfStars;
    }

    public void setNumberOfStars(int numberOfStars) {
        this.numberOfStars = numberOfStars;
    }

    public int getNumberOfSkulls() {
        return numberOfSkulls;
    }

    public void setNumberOfSkulls(int numberOfSkulls) {
        this.numberOfSkulls = numberOfSkulls;
    }

    public int getKarmaNegativeScore() {
        return karmaNegativeScore;
    }

    public void setKarmaNegativeScore(int karmaNegativeScore) {
        this.karmaNegativeScore = karmaNegativeScore;
    }

    public int getKarmaPositiveScore() {
        return karmaPositiveScore;
    }

    public void setKarmaPositiveScore(int karmaPositiveScore) {
        this.karmaPositiveScore = karmaPositiveScore;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public List<TaskCategoryFilter> getTaskCategories() {
        return taskCategories;
    }

    public void setTaskCategories(List<TaskCategoryFilter> taskCategories) {
        this.taskCategories = taskCategories;
    }
}
