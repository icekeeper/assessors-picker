package ru.yandex.assessor.picker.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.yandex.assessor.picker.entity.Assessor;


/**
 * Created by Степан on 02.11.2014.
 */
public interface AssessorRepository extends PagingAndSortingRepository<Assessor, Long> {
}
