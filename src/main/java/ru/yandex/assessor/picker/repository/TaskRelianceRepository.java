package ru.yandex.assessor.picker.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.yandex.assessor.picker.entity.TaskReliance;
import ru.yandex.assessor.picker.entity.compositeKey.CompositeRelianceKey;

/**
 * Created by Степан on 23.11.2014.
 */
public interface TaskRelianceRepository extends PagingAndSortingRepository<TaskReliance, CompositeRelianceKey> {
}
