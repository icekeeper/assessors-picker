package ru.yandex.assessor.picker.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.yandex.assessor.picker.entity.Region;

/**
 * Created by Степан on 26.10.2014.
 */
public interface RegionRepository extends PagingAndSortingRepository<Region, Long> {

}
