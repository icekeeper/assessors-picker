package ru.yandex.assessor.picker.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.yandex.assessor.picker.entity.TaskType;

/**
 * Created by Степан on 23.11.2014.
 */
public interface TaskTypeRepository extends PagingAndSortingRepository<TaskType, Long> {
}
