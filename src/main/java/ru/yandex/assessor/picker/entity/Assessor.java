package ru.yandex.assessor.picker.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Степан on 30.10.2014.
 */
@Entity
@Table(name = "assessor")
public class Assessor {
    @Id
    @JsonProperty("id")
    private long id;
    @Column
    @JsonProperty("region_id")
    private long regionId;
    @Column
    @JsonProperty("number_of_stars")
    private int numberOfStars;
    @Column
    @JsonProperty("number_of_skulls")
    private int numberOfSkulls;
    @Column
    @JsonProperty("karma_negative_score")
    private int karmaNegativeScore;
    @Column
    @JsonProperty("karma_positive_score")
    private int karmaPositiveScore;
    @Column
    @JsonProperty("online")
    private boolean online;

    @OneToMany(mappedBy = "assessor")
    @JsonProperty("categories-task")
    private List<TaskReliance> taskReliances;

    public List<TaskReliance> getTaskReliances() {
        return taskReliances;
    }

    public void setTaskReliances(List<TaskReliance> taskReliances) {
        this.taskReliances = taskReliances;
    }

    public Assessor() {
    }

    public Assessor(long id, long regionId, int numberOfStars, int numberOfSkulls, int karmaNegativeScore, int karmaPositiveScore, boolean online) {
        this.id = id;
        this.regionId = regionId;
        this.numberOfStars = numberOfStars;
        this.numberOfSkulls = numberOfSkulls;
        this.karmaNegativeScore = karmaNegativeScore;
        this.karmaPositiveScore = karmaPositiveScore;
        this.online = online;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public int getNumberOfStars() {
        return numberOfStars;
    }

    public void setNumberOfStars(int numberOfStars) {
        this.numberOfStars = numberOfStars;
    }

    public int getNumberOfSkulls() {
        return numberOfSkulls;
    }

    public void setNumberOfSkulls(int numberOfSkulls) {
        this.numberOfSkulls = numberOfSkulls;
    }

    public int getKarmaNegativeScore() {
        return karmaNegativeScore;
    }

    public void setKarmaNegativeScore(int karmaNegativeScore) {
        this.karmaNegativeScore = karmaNegativeScore;
    }

    public int getKarmaPositiveScore() {
        return karmaPositiveScore;
    }

    public void setKarmaPositiveScore(int karmaPositiveScore) {
        this.karmaPositiveScore = karmaPositiveScore;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
