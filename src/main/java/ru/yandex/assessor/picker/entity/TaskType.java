package ru.yandex.assessor.picker.entity;

import ru.yandex.assessor.picker.entity.compositeKey.CompositeTypeKey;

import javax.persistence.*;

/**
 * Created by Степан on 23.11.2014.
 */
@Entity
@Table(name = "task")
@IdClass(CompositeTypeKey.class)
public class TaskType {
    @Id
    @Column
    private int typeId;
    @Id
    @Column
    private int subtypeId;
    @Column
    private String typeNameRu;
    @Column
    private String typeNameEng;

    public TaskType() {
    }

    public TaskType(int typeId, int subtypeId, String typeNameRu, String typeNameEng) {
        this.typeId = typeId;
        this.subtypeId = subtypeId;
        this.typeNameRu = typeNameRu;
        this.typeNameEng = typeNameEng;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getSubtypeId() {
        return subtypeId;
    }

    public void setSubtypeId(int subtypeId) {
        this.subtypeId = subtypeId;
    }

    public String getTypeNameRu() {
        return typeNameRu;
    }

    public void setTypeNameRu(String typeNameRu) {
        this.typeNameRu = typeNameRu;
    }

    public String getTypeNameEng() {
        return typeNameEng;
    }

    public void setTypeNameEng(String typeNameEng) {
        this.typeNameEng = typeNameEng;
    }
}
