package ru.yandex.assessor.picker.entity.compositeKey;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.yandex.assessor.picker.entity.Assessor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Степан on 24.11.2014.
 */
@Embeddable
public class CompositeRelianceKey implements Serializable {

    private long assessor;

    private int typeId;

    private int subtypeId;

    public CompositeRelianceKey(long assessor, int typeId, int subtypeId) {
        this.assessor = assessor;
        this.typeId = typeId;
        this.subtypeId = subtypeId;
    }

    public CompositeRelianceKey() {
    }

    public long getAssessor() {
        return assessor;
    }

    public void setAssessor(long assessor) {
        this.assessor = assessor;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getSubtypeId() {
        return subtypeId;
    }

    public void setSubtypeId(int subtypeId) {
        this.subtypeId = subtypeId;
    }
}
