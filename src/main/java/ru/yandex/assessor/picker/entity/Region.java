package ru.yandex.assessor.picker.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Степан on 02.11.2014.
 */
@Entity
@Table(name = "coordinates")
public class Region {
    @Id
    @Column
    @JsonProperty("region_id")
    private long regionId;
    @Column
    @JsonProperty("region_name_ru")
    private String regionNameRu;
    @Column
    @JsonProperty("region_name_eng")
    private String regionNameEng;
    @Column
    @JsonProperty("latitude")
    private double latitude;
    @Column
    @JsonProperty("longitude")
    private double longitude;

    public Region() {
    }

    public Region(long regionId, String regionNameRu, String regionNameEng, double latitude, double longitude) {
        this.regionId = regionId;
        this.regionNameRu = regionNameRu;
        this.regionNameEng = regionNameEng;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public String getRegionNameRu() {
        return regionNameRu;
    }

    public void setRegionNameRu(String regionNameRu) {
        this.regionNameRu = regionNameRu;
    }

    public String getRegionNameEng() {
        return regionNameEng;
    }

    public void setRegionNameEng(String regionNameEng) {
        this.regionNameEng = regionNameEng;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
