package ru.yandex.assessor.picker.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.yandex.assessor.picker.entity.compositeKey.CompositeRelianceKey;

import javax.persistence.*;
import java.util.ArrayList;

/**
 * Created by Степан on 23.11.2014.
 */
@Entity
@Table(name = "taskReliance")
public class TaskReliance {
    //инициализируем асессора в конструкторе и добавляем его в базу данных

    @JsonIgnore
    @EmbeddedId
    CompositeRelianceKey key;
    @MapsId("assessor")
    @ManyToOne
    @JoinColumn(unique = false, nullable = false, name = "assessor_id", referencedColumnName = "id")
    @JsonIgnore
    private Assessor assessor;


    @Column(insertable = false, updatable = false)
    private int typeId;

    @Column(insertable = false, updatable = false)
    private int subtypeId;

    private double accuracy;

    public TaskReliance(Assessor assessor,CompositeRelianceKey key, double accuracy) {
        this.assessor = assessor;
        this.key = key;
        this.accuracy = accuracy;
    }



    public TaskReliance() {
    }

    public CompositeRelianceKey getKey() {
        return key;
    }

    public void setKey(CompositeRelianceKey key) {
        this.key = key;
    }

    public Assessor getAssessor() {
        return assessor;
    }

    public void setAssessor(Assessor assessor) {
        this.assessor = assessor;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getSubtypeId() {
        return subtypeId;
    }

    public void setSubtypeId(int subtypeId) {
        this.subtypeId = subtypeId;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }
}