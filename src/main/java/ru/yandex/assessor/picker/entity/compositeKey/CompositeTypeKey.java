package ru.yandex.assessor.picker.entity.compositeKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by Степан on 24.11.2014.
 */
@Embeddable
public class CompositeTypeKey implements Serializable {
    @Column
    private int typeId;
    @Column
    private int subtypeId;

    public CompositeTypeKey() {
    }

    public CompositeTypeKey(int typeId, int subtypeId) {
        this.typeId = typeId;
        this.subtypeId = subtypeId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getSubtypeId() {
        return subtypeId;
    }

    public void setSubtypeId(int subtypeId) {
        this.subtypeId = subtypeId;
    }
}

