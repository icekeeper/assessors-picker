CREATE DATABASE `yandex.assessors` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `yandex.assessors` ;
CREATE  TABLE IF NOT EXISTS `yandex.assessors`.`assessor` (

  `id` BIGINT NOT NULL ,

  `regionId` BIGINT NULL , INDEX i_region_id(regionId) ,

  `numberOfStars` INT NULL , INDEX i_number_of_stars(numberOfStars),

  `numberOfSkulls` INT NULL , INDEX i_number_of_skalls(numberOfSkulls),

  `karmaNegativeScore` INT NULL , INDEX i_karma_negative(karmaNegativeScore),

  `karmaPositiveScore` INT NULL , INDEX i_karma_positive(karmaPositiveScore),

  `online` TINYINT(1) NULL , INDEX i_online(online),


  PRIMARY KEY (`id`) );

  CREATE TABLE IF NOT EXISTS `yandex.assessors`.`coordinates` (

  `regionId` BIGINT NOT NULL ,

  `regionNameRu` VARCHAR(100) NULL , INDEX index_region_Name_Ru(regionNameRu),

  `regionNameEng` VARCHAR(100) NULL , INDEX index_region_Name_Eng(regionNameEng),

  `latitude` DOUBLE NULL , INDEX index_latitude(latitude),

  `longitude` DOUBLE NULL , INDEX index_longitude(longitude),

  PRIMARY KEY (`regionId`) );

  CREATE TABLE IF NOT EXISTS `yandex.assessors`.`task`(

  `typeId` INT NOT NULL ,

  `subtypeId` INT NOT NULL ,

  `typeNameRu` VARCHAR(100) NULL , INDEX index_type_Name_Ru(typeNameRu),

  `typeNameEng` VARCHAR(100) NULL , INDEX index_type_Name_Eng(typeNameEng)

  );

   CREATE TABLE IF NOT EXISTS `yandex.assessors`.`taskReliance`(

    `assessor_id` BIGINT NOT NULL ,

    `typeId` INT NOT NULL ,

    `subtypeId` INT NOT NULL ,

    `accuracy` DOUBLE NOT NULL ,INDEX i_accuracy(accuracy)
    );
