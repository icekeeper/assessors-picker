var app = app || {};

var task_categories = [];

app.TaskCategoriesCollectionView = Backbone.View.extend({
    el: '#task-categories-select',
    tagName: 'select',

    initialize: function() {
        this.collection = new app.TaskCategoriesCollection();

        var that = this;

        this.collection.fetch({reset: true}).done(function() {

            that.collection.each(function(reg) {
                var r = {
                    typeId: reg.get("typeId"),
                    typeNameRu: reg.get("typeNameRu"),
                    typeNameEng: reg.get("typeNameEng"),
                    child_task_categories: reg.get("child_task_categories")
                };

                task_categories.push(r);
            });
        });

        this.render();

        this.listenTo( this.collection, 'reset', this.render );
    },

    render: function() {
        this.collection.each(function( item ) {
            this.renderTaskCategory( item );
        }, this );
    },

    renderTaskCategory: function( item ) {
        var taskCategory = new app.TaskCategoryItemView({
            model: item
        });
        this.$el.append( taskCategory.render().el );
    }
});