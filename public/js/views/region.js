
var app = app || {};

app.RegionItemView = Backbone.View.extend({
    tagName: 'option',
    template: _.template( $( '#regionItemView' ).html() ),

    render: function() {
        this.setElement(this.template(this.model.attributes));

        return this;
    }
});