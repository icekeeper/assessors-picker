
var app = app || {};

app.TaskCategoryItemView = Backbone.View.extend({
    tagName: 'option',
    template: _.template( $( '#taskCategoryItemView' ).html() ),

    render: function() {
        this.setElement(this.template(this.model.attributes));

        return this;
    }
});