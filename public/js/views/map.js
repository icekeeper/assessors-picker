var app = app || {};

app.mapCollection = new app.MapCollection();

$( document ).ready(function() {

    app.initMap = function()
    {
        return new Datamap({
            element: document.getElementById("map-container"),
            projection: 'mercator',
            zoomConfig: {
                zoomOnClick: true,
                zoomFactor: 1.5
            },
            fills: {
                //defaultFill: "#C8D2D9"
                //defaultFill: "#617dcc"
            }
        });
    };

    app.dataMap = app.initMap();

    //var colors = d3.scale.category10();
    //
    //app.dataMap.updateChoropleth({
    //    USA: colors(Math.random() * 10),
    //    TUR: colors(Math.random() * 100),
    //    CAN: colors(Math.random() * 50),
    //    BLR: colors(Math.random() * 50),
    //    RUS: colors(Math.random() * 100),
    //    UKR: colors(Math.random() * 20),
    //    KAZ: colors(Math.random() * 10)
    //});

    $( "form#form-search-assessors" ).submit(function( event ) {
        app.mapCollection.fetch({reset: true}).done(function() {

            var regions = [];

            app.mapCollection.each(function(reg) {
                var r = {
                    region: reg.get("region"),
                    latitude: reg.get("latitude"), //X
                    longitude: reg.get("longitude"), //Y
                    radius: reg.get("percentOfAssessors") * 80
                };

                regions.push(r);
            });

            //draw bubbles for regions
            app.dataMap.bubbles(regions, {
                popupTemplate: function (geo, data) {
                    return ['<div class="hoverinfo">' +  data.region + '</div>'].join('');
                }
            });

        });

        event.preventDefault();
    });

    function updateMap()
    {
        $( "form#form-search-assessors").submit();
    }

    //Отображаем карту при открытии страницы
    updateMap();

    var refreshIntervalId = "";

    $(document).on("click","button.button-hide-page",function() {
        $("#search-assessors").addClass('hide');
        $("h1#h1-list-of-assessors").addClass('hide');

        app.navbar = $("html body div.navbar.navbar-inverse.navbar-fixed-top").html();
        $(".navbar-fixed-top").remove();

        $("svg.datamap").empty().remove();
        $(".datamaps-hoverover").remove();

        $("button.button-hide-page").remove();

        $("body").css('padding-top', 0).css('padding-bottom', 0);

        var w = $(window).width();
        var h = $(window).height();

        var basicRatio = 1140.0 / 815.0;

        if (w / h > basicRatio)
        {
            w = h * basicRatio;
        }
        else
        {
            h = w / basicRatio;
        }

        $('#map-container').css('width', w + 'px').css('height', h + 'px').css('margin-top', '5px')
                            .css('margin-left', 'auto').css('margin-right', 'auto');

        app.dataMap = app.initMap();

        var resultHtml = '<div class="show-page-button-and-form-update-interval"><button type="button" class="btn btn-default pull-right button-show-page">Показать меню</button>' +
                            $("#form-update-interval-html")[0].innerHTML +
                        '</div>';

        $("div.container").append(resultHtml);

        updateMap();

        var updateTime = 1000 * 60 * $("input#update-interval").val() || 1000 * 60;

        refreshIntervalId = setInterval(updateMap, updateTime);
    });

    $(document).on("change","input#update-interval",function() {
        clearInterval(refreshIntervalId);

        updateMap();

        var updateTime = 1000 * 60 * $("input#update-interval").val() || 1000 * 60;

        refreshIntervalId = setInterval(updateMap, updateTime);
    });

    $(document).on("submit","form.form-update-interval",function(e) {
        e.preventDefault();
    });


    $(document).on("click","button.button-show-page",function() {
        clearInterval(refreshIntervalId);

        $("#search-assessors").removeClass('hide');
        $("h1#h1-list-of-assessors").removeClass('hide');

        $("body").prepend('<div class="navbar navbar-inverse navbar-fixed-top">' + app.navbar + '</div>');

        $("svg.datamap").empty().remove();
        $(".datamaps-hoverover").remove();

        $(".show-page-button-and-form-update-interval").remove();

        $("body").css('padding-top', '50px').css('padding-bottom', '40px');

        $('#map-container').css('width', '1140px').css('height', '815px').css('margin-top', '30px')
            .css('margin-left', 'inherit').css('margin-right', 'inherit');

        app.dataMap = app.initMap();

        $("div#search-assessors").before('<button type="button" class="btn btn-success pull-right button-hide-page">Растянуть карту</button>');

        updateMap();
    });
});