var app = app || {};

app.assessors = new app.AssessorsCollection();

app.grid = new Backgrid.Grid({
    columns: [{
        name: "",
        cell: "select-row",
        headerCell: "select-all"
    }, {
        name: "login",
        label: "Login",
        cell: Backgrid.StringCell.extend({ className: "string-cell login" }),
        sortable: false,
        editable: false
    }, {
        name: "region_id",
        label: "Регион",
        cell: Backgrid.StringCell.extend({ className: "string-cell region_id" }),
        sortable: false,
        editable: false
    }, {
        name: "number_of_skulls",
        label: "Черепа",
        cell: Backgrid.StringCell.extend({ className: "string-cell number_of_skulls" }),
        sortable: false,
        editable: false
    }, {
        name: "number_of_stars",
        label: "Звёзды",
        cell: Backgrid.StringCell.extend({ className: "string-cell number_of_stars" }),
        sortable: false,
        editable: false
    }, {
        name: "karma_positive_score",
        label: "Плюсы",
        cell: Backgrid.StringCell.extend({ className: "string-cell karma_positive_score" }),
        sortable: false,
        editable: false
    }, {
        name: "experience_in_months",
        label: "Опыт работы",
        cell: Backgrid.StringCell.extend({ className: "string-cell experience_in_months" }),
        sortable: false,
        editable: false
    },{
        name: "evaluation_per_month",
        label: "Оценок в мес.",
        cell: Backgrid.StringCell.extend({ className: "string-cell evaluation_per_month" }),
        sortable: false,
        editable: false
    }, {
        name: "karma_negative_score",
        label: "Минусы",
        cell: Backgrid.StringCell.extend({ className: "string-cell karma_negative_score" }),
        sortable: false,
        editable: false
    },{
        name: "online",
        label: "Online",
        cell: Backgrid.StringCell.extend({ className: "string-cell online" }),
        sortable: false,
        editable: false
    }],

    collection: app.assessors
});

app.paginator = new Backgrid.Extension.Paginator({
    collection: app.assessors
});


$( document ).ready(function() {

    function getAssessors(sizeOfPage)
    {
        if (sizeOfPage)
        {
            app.assessors.state.pageSize = sizeOfPage;
        }
        else
        {
            app.assessors.state.pageSize = app.assessors.defaultPageSize;
        }

        app.assessors.state.currentPage = 1;

        var regions = {};

        $("#regions-select > option").each(function() {
            regions[this.value] = this.text;
        });

        app.regions = regions;

        $("#grid").empty();
        $("#grid").append(app.grid.render().$el);
        $("#paginator").append(app.paginator.render().$el);

        app.assessors.fetch({reset: true});
    }

    $( "form#form-search-assessors" ).submit(function( event ) {
        $("#h2-table-2").removeClass('hide');
        $("button.donwload-assessors").removeClass('hide');
        $("button.send-email").removeClass('hide');

        getAssessors();

        event.preventDefault();
    });

    $(document).on( "click", "a.get-all-assessors", function(event){
        var maxPossibleCountOfAssessors = 99999;

        getAssessors(maxPossibleCountOfAssessors);

        event.preventDefault();
    });

    $("button.donwload-assessors").click(function() {
        var download_assessors_collection = new app.AssessorsCollection();
        download_assessors_collection.state.pageSize = 9999999;

        download_assessors_collection.fetch({reset: true}).done(function() {

            var download_assessors = [];

            download_assessors_collection.each(function(reg) {
                var r = {
                    login: reg.get("login")
                };

                download_assessors.push(r.login + "\r\n");
            });

            var blob = new Blob(download_assessors, {type: "text/plain;charset=utf-8"});
            saveAs(blob, "assessors.txt");

        });
    });

    $("button.send-email").click(function(event) {
        var newHref = "mailto:";

        var emailSuffix = "@yandex-team.ru";

        var haveEmails = false;

        var selectedModels = app.grid.getSelectedModels();
        _.each(selectedModels, function (model) {
            haveEmails = true;

            newHref += model.attributes.login + emailSuffix;
            newHref += ",";
        });

        newHref = newHref.substring(0, newHref.length - 1);

        if (haveEmails) {
            window.open(newHref, '_blank');
        }
    });

    function insertTaskCategoryItem(child_id, child_task_category_title, parent_task_category_id) {
        var html = '<div class="selected-task-category">' +
            '<option name="child-task-category-item" value="' + child_id + '">' + child_task_category_title + '</option>' +
            '<input name="parent_id" type="hidden" value="' + parent_task_category_id + '" />' +
            '<input value="0" required="required" min="0" max="1" step="0.01" placeholder="Точность" type="number" name="accuracy-of-job" class="accuracy-of-job form-control input-sm" />' +

            '<button type="button" class="btn btn-xs btn-danger remove-task-category-item">' +
            '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' +
            '</button>' +
            '</div>';

        $("#selected-task-categories").append(html);
    }

    var parent_task_category_id = 0;

    function taskItemChangeEventHandler(val)
    {
        parent_task_category_id = val;

        for (var i = 0; i < task_categories.length; ++i)
        {
            var task_category = task_categories[i];

            if (task_category.typeId == val)
            {
                var childs = task_category.child_task_categories;

                if (childs.length > 0) {

                    $(".child-task-categories-item").removeClass('hide');

                    $('#child-task-categories-select')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option selected> </option>')
                        .val('whatever');

                    for (var j = 0; j < childs.length; ++j) {
                        var child = childs[j];

                        $("#child-task-categories-select").append('<option value="' + child.subtypeId + '">' + child.subtypeNameRu + '</option>');
                    }
                }
                else {
                    insertTaskCategoryItem(-1, task_category.typeNameRu, task_category.typeId);
                }
            }
        }

        $('#task-categories-select').find(':nth-child(1)').prop('selected', true); // To select via index
    }

    function childTaskItemChangeEventHandler(val) {

        var child_task_category_title = "";

        for (var i = 0; i < task_categories.length; ++i)
        {
            var task_category = task_categories[i];

            if (task_category.typeId == parent_task_category_id)
            {
                var childs = task_category.child_task_categories;

                for (var j = 0; j < childs.length; ++j)
                {
                    var child = childs[j];

                    if (child.subtypeId == val)
                    {
                        child_task_category_title = child.subtypeNameRu;
                    }
                }
            }
        }

        insertTaskCategoryItem(val, child_task_category_title, parent_task_category_id);

        $(".child-task-categories-item").addClass('hide');
    }

    //workaround for http://stackoverflow.com/questions/27081848/change-event-on-dynamic-select-does-not-work-in-firefox
    if (navigator.userAgent.indexOf("Firefox") > -1) {
        $(document).on('click', "#task-categories-select option", function () {
            taskItemChangeEventHandler(this.value);
        });
    } else {
        $(document).on('change', "#task-categories-select", function () {
            taskItemChangeEventHandler(this.value);
        });
    }

    $(document).on('change', "#child-task-categories-select", function () {
        childTaskItemChangeEventHandler(this.value);
    });

    $(document).on('click', "button.remove-task-category-item", function() {
        $(this).parent().remove();
    });
});