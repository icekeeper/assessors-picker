var app = app || {};

app.RegionsCollectionView = Backbone.View.extend({
    el: '#regions-select',
    tagName: 'select',

    initialize: function() {
        this.collection = new app.RegionsCollection();
        this.collection.fetch({reset: true});
        this.render();

        this.listenTo( this.collection, 'reset', this.render );
    },

    render: function() {
        this.collection.each(function( item ) {
            this.renderRegion( item );
        }, this );
    },

    renderRegion: function( item ) {
        var region = new app.RegionItemView({
            model: item
        });
        this.$el.append( region.render().el );
    }
});