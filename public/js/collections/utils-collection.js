
var app = app || {};

app.getAssessorsUrl = function(bUrl) {
    var baseUrl = bUrl;

    var online = $('input[name=onlineChoice]:checked', "form#form-search-assessors").val();
    var region = $( "#regions-select" ).val();

    var score_positive_number = $('#score-positive-number').val();
    var score_negative_number = $('#score-negative-number').val();

    if (!score_negative_number)
    {
        score_negative_number = "9999";
    }

    var skull_from_number = $('#skull-from-number').val();
    var skull_to_number = $('#skull-to-number').val();

    if (!skull_to_number)
    {
        skull_to_number = "9999";
    }

    var star_from_number = $('#star-from-number').val();
    var star_to_number = $('#star-to-number').val();

    if (!star_to_number)
    {
        star_to_number = "9999";
    }

    var experience_from = $('#experience-from').val();
    var experience_to = $('#experience-to').val();

    if (!experience_to)
    {
        experience_to = "9999";
    }

    var evaluation_per_month = $('#evaluation-per-month').val();

    var task_categories_filter = [];

    $('#selected-task-categories').find('> div').each(function () {
        var childDiv = $(this);
        var child_id = childDiv.find("option").val();
        var parent_id = childDiv.find("input[name=parent_id]").val();
        var accuracy = childDiv.find("input[name=accuracy-of-job]").val();

        task_categories_filter.push({
            typeId: parent_id,
            subtypeId: child_id,
            accuracy: accuracy
        });
    });

    var task_categories_filter_json = JSON.stringify(task_categories_filter);

    var parameters = $.param({
        score_positive_number: score_positive_number,
        score_negative_number: score_negative_number,
        skull_from_number: skull_from_number,
        skull_to_number: skull_to_number,
        star_from_number: star_from_number,
        star_to_number: star_to_number,
        experience_from: experience_from,
        experience_to: experience_to,
        evaluation_per_month: evaluation_per_month,
        online: online
    });

    if (task_categories_filter.length > 0)
    {
        parameters += ("&" + $.param( {"task_categories" : task_categories_filter_json} ));
    }

    if (region)
    {
        parameters += ("&" + $.param( {"region_id" : region} ));
    }

    var url = baseUrl + "?" + parameters;

    return url;
};