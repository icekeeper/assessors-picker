
var app = app || {};

app.AssessorsCollection = Backbone.PageableCollection.extend({

    model: app.Assessor,

    url: function() {
        return app.getAssessorsUrl("/assessors");
        //var u = app.getAssessorsUrl("http://localhost:3000/assessors");
        //return u;
    },

    defaultPageSize: 15,

    // Initial pagination states
    state: {
        pageSize: this.defaultPageSize,
        firstPage: 1,
        currentPage: 1
    },

    // You can remap the query parameters from `state` keys from
    // the default to those your server supports
    queryParams: {
        currentPage: "page",
        pageSize: "size",

        totalElements: null,
        totalRecords: null
    },

    // get the state from API result
    parseState: function (resp, queryParams, state, options) {
        return {totalRecords: resp.totalElements};
    },

    // get the actual records
    parseRecords: function (resp, options) {
        return resp.content;
    }

});