
var app = app || {};

app.RegionsCollection = Backbone.Collection.extend({
    model: app.Region,
    url: function() {
        return "/regions";
        //return "http://localhost:3000/regions"
    }
});