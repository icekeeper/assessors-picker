
var app = app || {};

app.MapCollection = Backbone.Collection.extend({
    model: app.Map,

    url: function() {
        var u = app.getAssessorsUrl("/map");
        //var u = app.getAssessorsUrl("http://localhost:3000/map");
        return u;
    }

});