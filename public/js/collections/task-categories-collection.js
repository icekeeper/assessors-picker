
var app = app || {};

app.TaskCategoriesCollection = Backbone.Collection.extend({
    model: app.TaskCategory,
    url: function() {
        //return "http://localhost:3000/task-categories";
        return "/task-categories";
    }
});