var app = app || {};

app.Assessor = Backbone.Model.extend({
    defaults: {
        id: '0',
        login: '',
        region_id: '0',
        online: false,
        number_of_stars: 0,
        number_of_skulls: 0,
        karma_positive_score: 0,
        karma_negative_score: 0,
        evaluation_per_month: 0,
        experience_in_months: 0,
        task_categories: []
    },
    parse: function(resp, xhr) {
        if (resp.online)
        {
            resp.online = "On-line";
        }
        else
        {
            resp.online = "Off-line";
        }

        resp.login = resp.id + "_login";
        resp.region_id = app.regions[resp.region_id];

        return resp;
    }
});