var app = app || {};

app.Region = Backbone.Model.extend({
    defaults: {
        region_id: '0',
        region_name_ru: '',
        latitude: 0,
        longitude: 0,
        region_name_eng: ''
    }
});