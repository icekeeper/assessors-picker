var app = app || {};

app.Map = Backbone.Model.extend({
    defaults: {
        region: '',
        latitude: 0, //X
        longitude: 0, //Y
        percentOfAssessors: 0
    }
});