var app = app || {};

app.TaskCategory = Backbone.Model.extend({
    defaults: {
        typeId: '0',
        typeNameRu: '',
        typeNameEng: '',
        child_task_categories: []
    }
});