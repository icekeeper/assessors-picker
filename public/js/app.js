
var app = app || {};

$(function() {
    new app.RegionsCollectionView();
    new app.TaskCategoriesCollectionView();
});